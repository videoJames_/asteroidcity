﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BoatHandling : MonoBehaviour
{

    //config
    public float wallAvoidDist,//how far to raycast looking for a wall to push away from
        wallTurnDist,//how far to raycast looking for a wall to turn away from
        wallRepelStrength,//how much force to apply when pushing away from a wall
        wallTurnStrength,//how much torque to apply when turning away from a wall
        verticalVelocityDamping,//how much upward drag to apply when out of the water
        wallCollidePush,//how much to move when hit a wall
        wallDownforce,//how much downforce to apply when hitting a wall
        turnAssistDist,
        turnAssistStrength,
        turnAssistPushStrength,
        turnAssistResponsiveness,//how fast the turnassist increases when steering away from wall
        turnAssistExitResponse,//percentage of turn assist response speed when exiting vs entering
        assistVelocityThreshold;//velocity under which not to apply turn assistance and wall avoidance


    [Range(0, 1)]
    public float wallCollideRotate,//what percent to change the rotation when hitting a wall
        wallCollideRedirect,//how much to rotate velocity direction when hitting a wall
        wallVelocityDamping,//how much to change velocity magnitude
        turnSlideFactor;//how much to change drag when steering
        
    [Range(.9f,1f)]public float hardSteerDecay;//how much to atrophy speed when careening towards a wall

    //state
    float averageTurnInput, averageAccelInput, currentTurnAssist, timeSinceLastWallCollision;
    Vector3 averageVelocity;

    //bookkeeping
    Buoyancy b;
    PartBasedBoat p;

    void OnCollisionEnter(Collision col)
    {
        if (col.gameObject.layer == LayerMask.NameToLayer("Wall"))
        {
            if(b.touchingWater > .2f)
            {

                if(timeSinceLastWallCollision > .2f && b.rb.velocity.magnitude > assistVelocityThreshold)
                {
                    Vector3 lookDir = Vector3.Cross(col.contacts[0].normal, b.currentGravity);//calculate new rotation based on wall angle
                    lookDir = Vector3.Project(b.t.forward, lookDir); //constrain rotation to avoid 180 degree spins
                    lookDir = Vector3.Lerp(b.t.forward, lookDir, wallCollideRotate);
                    transform.rotation = Quaternion.LookRotation(lookDir);


                    Vector3 redirectDir = Vector3.Cross(col.contacts[0].normal,b.currentGravity);//new velocity direction
                    redirectDir = Vector3.Project(b.rb.velocity, redirectDir);
                    redirectDir = redirectDir.normalized * b.rb.velocity.magnitude;
                    redirectDir = Vector3.Lerp(b.rb.velocity, redirectDir, wallCollideRedirect);
                    b.rb.velocity = redirectDir;
                    b.rb.angularVelocity = Vector3.zero;
                    Debug.DrawRay(transform.position, redirectDir, Color.magenta, 5);
                }

                b.rb.AddForce(col.contacts[0].normal * Mathf.Clamp01(timeSinceLastWallCollision) * wallCollidePush * 100, ForceMode.Acceleration);//push away from wall
            }
            timeSinceLastWallCollision = 0f;

            //add extra downforce
            Vector3 dragDir = Vector3.Reflect(b.rb.velocity.normalized, -b.currentGravity);

            float strength = (Vector3.Angle(b.rb.velocity, b.currentGravity) - 90) / 50;//apply most drag when angle is most extreme. min is around 60%
            strength = Mathf.Lerp(0, strength, Vector3.Project(b.rb.velocity, -b.currentGravity).magnitude / 30);

            strength *= (wallDownforce);

            dragDir = Vector3.Scale(dragDir, Vector3.one * strength);
            dragDir = (dragDir + b.rb.velocity).normalized * b.rb.velocity.magnitude;

            dragDir *= wallVelocityDamping; //decay velocity
            b.rb.velocity = dragDir;

        }
    }

    void Update()
    {
        //decrease drag based on input to get nice drifty swooshiness
        b.waterDragModifier *= Mathf.Clamp(1 - (Mathf.Abs(averageAccelInput) * Mathf.Abs(averageTurnInput * turnSlideFactor)),0,100);
            timeSinceLastWallCollision += Time.deltaTime;
    }

    void Start()
    {
        b = GetComponent<Buoyancy>();
        p = GetComponent<PartBasedBoat>();
    }

    void FixedUpdate()
    {
        if (b.rb.velocity.magnitude > assistVelocityThreshold)
        {
            AvoidWalls();
        }
        else
        {
            LimitVerticalVelocity();
        }

        if(b.rb.velocity.magnitude > assistVelocityThreshold)
        {
            TurnAssist();
        }

        averageAccelInput = Mathf.Lerp(averageAccelInput, p.accelInput, .1f);
        averageTurnInput = Mathf.Lerp(averageTurnInput, p.turnInput, .3f);//used to determine wall repel torque dierction
        averageVelocity = Vector3.Lerp(averageVelocity, b.rb.velocity, .4f);
    }

    void LimitVerticalVelocity()
    {
        
        if (Vector3.Angle(averageVelocity, b.currentGravity) > 90)//apply extra downward drag when not touching water
        {
            Vector3 dragDir = Vector3.Reflect(b.rb.velocity.normalized, -b.currentGravity);


            float strength = (Vector3.Angle(b.rb.velocity, b.currentGravity) - 90) / 50;//apply most drag when angle is most extreme. min is around 60%
            strength = Mathf.Lerp(0, strength, Vector3.Project(b.rb.velocity, -b.currentGravity).magnitude / 30);

            strength *= (verticalVelocityDamping / 100);

            dragDir = Vector3.Scale(dragDir, Vector3.one * strength);
            dragDir = (dragDir + b.rb.velocity).normalized * b.rb.velocity.magnitude;
            b.rb.velocity = dragDir;
        }
    }

    void TurnAssist()
    {
        RaycastHit hit;
        if(b.touchingWater > .2f && Mathf.Abs(averageTurnInput) > .2f && Physics.SphereCast(transform.position,15f,b.rb.velocity.normalized * turnAssistDist, out hit, turnAssistDist, 1 << LayerMask.NameToLayer("Wall")))
        {
            float num = Vector3.Dot(hit.normal, Vector3.Cross(b.rb.velocity, b.currentGravity)); //facing left or right
            if (Mathf.RoundToInt(Mathf.Sign(num)) == Mathf.RoundToInt(Mathf.Sign(averageTurnInput)))//if we're turning away from the wall
            {
                b.rb.AddTorque(b.lastWaterNormal * Mathf.Sign(averageTurnInput) * turnAssistStrength * (1 - (hit.distance / turnAssistDist)), ForceMode.Acceleration);
                currentTurnAssist = Mathf.Lerp(currentTurnAssist, ((1 - (hit.distance / turnAssistDist))) * ((1 - (hit.distance / turnAssistDist))) * turnAssistPushStrength * -Mathf.Sign(num), Time.deltaTime * turnAssistResponsiveness); //increase current turn assist over time
            }
            else
            {
                currentTurnAssist = Mathf.Lerp(currentTurnAssist, 0, Time.deltaTime * turnAssistResponsiveness * turnAssistExitResponse);
            }
        }
        else
        {
            currentTurnAssist = Mathf.Lerp(currentTurnAssist, 0, Time.deltaTime * turnAssistResponsiveness * turnAssistExitResponse);
        }
            Vector3 dir = b.rb.velocity;
            dir = Quaternion.AngleAxis( currentTurnAssist, b.currentGravity) * dir;
            b.rb.velocity = dir * hardSteerDecay;

    }

    void AvoidWalls()
    {
        RaycastHit hit;
        int origin = 0;
        Vector3 pushVector, pushPoint;

        Vector3[] worldPoints = new Vector3[5];
        for (int i = 0; i < 5; i++)
        {
            worldPoints[i] = b.t.TransformPoint(b.localHoverPoints[i]); // these are the hover points in world space
        }



        for (int i = 1; i < worldPoints.Length; i++) //loop through the points and do 2 casts looking for walls
        {
            switch (i)
            {
                case 1:
                    origin = 2;
                    break;
                case 2:
                    origin = 1;
                    break;
                case 3:
                    origin = 4;
                    break;
                case 4:
                    origin = 3;
                    break;
            }


            //raycasts out from the sides
            if (Physics.Raycast(worldPoints[origin], worldPoints[i] - worldPoints[origin], out hit, wallAvoidDist, 1 << LayerMask.NameToLayer("Wall")))
            {
                float s = (1 - Mathf.Clamp01((hit.distance / wallAvoidDist) + .1f)) * wallRepelStrength;
                pushVector = Vector3.Cross(hit.normal, b.lastWaterNormal);
                if (Vector3.Dot(transform.forward, pushVector) < 0)
                {
                    s = -s;
                }
                pushVector = -Vector3.Cross(pushVector, b.lastWaterNormal).normalized * Mathf.Abs(s);

                pushPoint = worldPoints[i];

                //if wall was found, apply force pushing away from the wall at both the closest hoverpoint and the center of the transform
                b.rb.AddForce(pushVector * b.touchingWater, ForceMode.Acceleration);

            }


            switch (i)
            {
                case 1:
                    origin = 3;
                    break;
                case 2:
                    origin = 4;
                    break;
            }

            //raycast diagonally in an X shape
            if (i < 3 && Physics.Raycast(b.t.position, worldPoints[i] - worldPoints[0], out hit, wallTurnDist, 1 << LayerMask.NameToLayer("Wall")) || (Physics.Raycast(worldPoints[origin], worldPoints[i] - worldPoints[origin], out hit, wallAvoidDist, 1 << LayerMask.NameToLayer("Wall"))))
            {
                float s = (1 - Mathf.Clamp01((hit.distance / wallAvoidDist) + .1f));
                pushVector = Vector3.Cross(hit.normal, b.lastWaterNormal);
                if (Vector3.Dot(transform.forward, pushVector) < 0)
                {
                    s = -s;
                }
                pushVector = -Vector3.Cross(pushVector, b.lastWaterNormal).normalized * Mathf.Abs(s * wallRepelStrength);

                pushPoint = worldPoints[i];


                //apply torque to turn away from the wall, depending on the speed & turn input
                if (Mathf.Abs(averageTurnInput) > .3f)
                {
                    if (s < 0 && averageTurnInput > 0)
                    {
                        s = -s;
                    }
                    else if (s > 0 && averageTurnInput < 0)
                    {
                        s = -s;
                    }
                }

                b.rb.AddTorque(b.lastWaterNormal * s * wallTurnStrength * b.touchingWater, ForceMode.Acceleration);
            }
        }
    }

}
