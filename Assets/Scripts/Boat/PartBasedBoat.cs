﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

//manages boat parts, upgrades, and behaviors
[RequireComponent(typeof(Buoyancy))]
[DisallowMultipleComponent]
public class PartBasedBoat : MonoBehaviour
{
    //config
    public List<BoatPart> allParts;

    //bookkeeping
    public Transform tiltTransform;
    [HideInInspector]public Buoyancy boatPhys;

    //state
    public List<BoatPartData> allPartData = new List<BoatPartData>();
    [HideInInspector]public float turnInput, accelInput;
    bool partListDirty = false; //set to true after the part list has been changed, set to false after all parts are reinitialized
    Vector3 startPos;


    void MakeStateData()
    {
        allPartData.Clear();
        foreach(BoatPart part in allParts) // populate list of new data objects
        {
            allPartData.Add(part.CreateBoatPartData());
        }

        for(int i = 0; i < allPartData.Count; i++)//initialize each part
        {
            allParts[i].Initialize(allPartData[i], this);
        }
    }

    void Initialize()
    {
        boatPhys = GetComponent<Buoyancy>();
        MakeStateData();
        partListDirty = false;
    }

    void Start()
    {
        Initialize();
        startPos = transform.position;
    }

    void Update()
    {
        if(Input.GetKeyDown(KeyCode.Space))
        {
            transform.position = startPos;
        }

        turnInput = Input.GetAxis("Horizontal");//replace with input class
        accelInput = Input.GetAxis("Vertical");

        for(int i = 0; i < allPartData.Count; i++)
        {
            allParts[i].UpdatePart(allPartData[i], this);
        }
    }

    void FixedUpdate()
    {
        for (int i = 0; i < allPartData.Count; i++)
        {
            allParts[i].FixedUpdatePart(allPartData[i], this);
        }
    }
}
