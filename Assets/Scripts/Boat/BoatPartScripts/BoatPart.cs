﻿using System.Collections;
using System;
using System.Collections.Generic;
using UnityEngine;

public abstract class BoatPart : ScriptableObject
{
    //what belongs in BoatParts: global data, config data (maxSpeed, boostRechargeRate), and unique behavior (ApplyTorque(), Jump(), Curse()) 

    public abstract BoatPartData CreateBoatPartData(); //called when first added to allboatparts after refresh of parts list
    public virtual void Initialize(BoatPartData partData, PartBasedBoat p) {}    //called immediately after all boat part data has been created
    public virtual void UpdatePart(BoatPartData partData, PartBasedBoat p) { }
    public virtual void FixedUpdatePart(BoatPartData partData, PartBasedBoat p) { }
}
public abstract class BoatPartData { }//empty base class for all state data objects


public abstract class EnginePart : BoatPart//config data fields that all EnginePart instances have
{
    public float maxSpeed;
}
public abstract class EngineData : BoatPartData//holds any state data that all EnginePart instances have
{
    public float currentThrust;
    public EnginePart boatPart;//ref to corresponding ScriptableObject
}

public abstract class RudderPart : BoatPart
{
    public float baseTurnSpeed;
}
public abstract class RudderData : BoatPartData
{
    public float currentTurnStrength, currentTurnRate;//strength is where on the torqueCurve we are evaluating
    public RudderPart boatPart;
}