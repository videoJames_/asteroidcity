﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BoostCharge_TimeData : BoatPartData
{
    public EngineData engineData;
    public List<BoostTank_Data> boostTanks;
}

[CreateAssetMenu(menuName = "BoatParts/Boost/Time Recharge")]
public class BoostCharge_Time : BoatPart
{
    public float rechargeRate;

    public override BoatPartData CreateBoatPartData()
    {
        return new BoostCharge_TimeData();
    }

    public override void Initialize(BoatPartData partData, PartBasedBoat p)
    {
        BoostCharge_TimeData data = partData as BoostCharge_TimeData;
        data.engineData = p.allPartData.Find(x => x is EngineData) as EngineData; // find engine so we can read its speed (will return the first engine on the list)

        data.boostTanks = new List<BoostTank_Data>();
        List<BoatPartData> tankData = p.allPartData.FindAll(x => x is BoostTank_Data);
        foreach (BoatPartData d in tankData)
        {
            data.boostTanks.Add(d as BoostTank_Data); // get all tank data
        }
    }

    public override void UpdatePart(BoatPartData partData, PartBasedBoat p)
    {
        BoostCharge_TimeData data = partData as BoostCharge_TimeData;
        if(!Input.GetKey(KeyCode.X) && !Input.GetKey(KeyCode.C))
        {
            for (int i = data.boostTanks.Count - 1; i >= 0; i--) //refills tanks up to capacity, one at a time
            {
                if (data.boostTanks[i].currentLevel < data.boostTanks[i].boostPart.capacity)
                {
                    data.boostTanks[i].currentLevel += rechargeRate * Time.deltaTime;
                    i = -1;
                }
            }
        }

    }
}
