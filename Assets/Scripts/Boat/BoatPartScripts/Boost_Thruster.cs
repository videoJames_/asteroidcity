﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Boost_ThrusterData : BoatPartData
{
    public EngineData engineData;
    public List<BoostTank_Data> boostTanks;
}

[CreateAssetMenu(menuName = "BoatParts/Boost/Thruster")]
public class Boost_Thruster : BoatPart
{
    //config
    public float burnRate, boostStrength;
    public Vector3 boostDir;

    public override BoatPartData CreateBoatPartData()
    {
        return new Boost_ThrusterData();
    }

    public override void Initialize(BoatPartData partData, PartBasedBoat c)
    {
        Boost_ThrusterData data = partData as Boost_ThrusterData;
        data.engineData = c.allPartData.Find(x => x is EngineData) as EngineData; // find engine so we can read its speed (will return the first engine on the list)

        data.boostTanks = new List<BoostTank_Data>();
        List<BoatPartData> tankData = c.allPartData.FindAll(x => x is BoostTank_Data);
        foreach (BoatPartData d in tankData)
        {
            data.boostTanks.Add(d as BoostTank_Data); // get all tank data
        }

    }

    public override void UpdatePart(BoatPartData partData, PartBasedBoat c)
    {
        Boost_ThrusterData data = partData as Boost_ThrusterData;

        int tankToUse = -1;
        //iterate through all tanks and look for first one with fluid left
        for (int i = data.boostTanks.Count - 1; i >= 0; i--)
        {
            if (data.boostTanks[i].currentLevel > 0)
            {
                tankToUse = i;
                i = -1;
            }
        }

        if (tankToUse >= 0)
        {
            if (Input.GetKey(KeyCode.C))
            {
                data.boostTanks[tankToUse].currentLevel -= Time.deltaTime * burnRate;
                c.boatPhys.rb.AddForce(c.boatPhys.t.TransformDirection(boostDir) * boostStrength * 100 * Time.deltaTime,ForceMode.Acceleration);
            }
        }
    }
}
