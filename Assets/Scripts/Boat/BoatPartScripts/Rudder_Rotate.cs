﻿    using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Rudder_RotateData : RudderData
{
    //state data
    public float lastTurnInput;
    public Vector3 lastVelocity;//average of previous frames
    public EngineData engineData;
}

//a rudder that applies torque to the boat based on input
[CreateAssetMenu(menuName = "BoatParts/Rudders/Torque")]
public class Rudder_Rotate : RudderPart
{
    //config
    public AnimationCurve torqueCurve;
    public float maxTurnSpeedTime = 3.5f,
        airTurnControl = .35f,//multiplier
        counterTurnForce = .4f,
        turnDecay = .6f; 

    public override BoatPartData CreateBoatPartData() { return new Rudder_RotateData(); }

    public override void Initialize(BoatPartData partData, PartBasedBoat c)
    {
        Rudder_RotateData data = partData as Rudder_RotateData;
        data.engineData = c.allPartData.Find(x => x is EngineData) as EngineData;//find engine so we can read its speed (will return the first engine on the list)
    }

    public override void UpdatePart(BoatPartData partData, PartBasedBoat c)
    {
        Rudder_RotateData data = partData as Rudder_RotateData;

        // how much will we be able to turn (0 to 1 ish, for use on the torqueChart
        float decayRate = -999f;//used to push values back down the torqueChart
        if (c.boatPhys.lastVelocity.magnitude > c.boatPhys.rb.velocity.magnitude * 1.2f || Mathf.Abs(c.accelInput) < .5f) // if decelerating
        {
            decayRate = 3.2f;
        }
        else if (c.boatPhys.touchingWater < .3f) // if falling
        {
            decayRate = .8f;
        }
        else if (Mathf.Abs(c.turnInput) > .01f && Mathf.Sign(c.turnInput) != Mathf.Sign(data.lastTurnInput)) //if switching directions
        {
            decayRate = .8f;
        }
        else if (Mathf.Abs(data.lastTurnInput) > .6f && Mathf.Abs(data.lastTurnInput) < Mathf.Abs(c.turnInput))//if turning again in the same direction
        {
            decayRate = .5f;
        }
        decayRate *= turnDecay;

        float n = c.boatPhys.rb.velocity.magnitude/data.engineData.boatPart.maxSpeed;

        if (decayRate > 0)
            data.currentTurnStrength = Mathf.Lerp(data.currentTurnStrength, 0, Time.deltaTime * decayRate);
        else
            data.currentTurnStrength = Mathf.Lerp(data.currentTurnStrength, Mathf.Abs(data.engineData.currentThrust / (data.engineData.boatPart.maxSpeed * .3f)), Time.deltaTime / maxTurnSpeedTime);
        data.currentTurnStrength = n;
        //input
        if (c.boatPhys.touchingWater < .3f)
        {
            c.turnInput *= airTurnControl;
        }

        //tilt the transform to simulate pitch (yuck)
        if (c.tiltTransform != null)
            c.tiltTransform.localRotation = Quaternion.Slerp(c.tiltTransform.localRotation, Quaternion.Euler(new Vector3(0, 0, -c.turnInput * 45)), Time.deltaTime * 1.65f);

        //apply counterturn when not steering to cancel out angular velocity
        data.lastTurnInput = Mathf.Lerp(data.lastTurnInput, c.turnInput, Time.deltaTime * 2.5f);
        if (Mathf.Abs(c.turnInput) < .2f)
        {
            c.turnInput = -data.lastTurnInput * counterTurnForce;
        }
    }

    public override void FixedUpdatePart(BoatPartData partData, PartBasedBoat c)
    {
        Rudder_RotateData data = partData as Rudder_RotateData;

        c.boatPhys.rb.AddRelativeTorque(new Vector3(0, 1, 0) * c.turnInput * (baseTurnSpeed / 100) * torqueCurve.Evaluate(data.currentTurnStrength) * data.currentTurnRate, ForceMode.VelocityChange);
        data.lastVelocity = Vector3.Lerp(data.lastVelocity, c.boatPhys.rb.velocity, .1f);
        data.currentTurnRate = 1;

    }

}
