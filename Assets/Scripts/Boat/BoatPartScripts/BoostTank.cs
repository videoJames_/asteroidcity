﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BoostTank_Data :BoatPartData
{
    public BoostTank boostPart;
    public float currentLevel;// how much is in the tank
    public BoostTank_Data(BoostTank p) { boostPart = p; }
}

[CreateAssetMenu(menuName = "BoatParts/Boost/Tank")]
public class BoostTank : BoatPart
{
    public float capacity; //scale should be ten times duration in seconds (at a base burn rate of 10 per second)

    public override BoatPartData CreateBoatPartData()
    {
        return new BoostTank_Data(this);
    }

    public override void Initialize(BoatPartData partData, PartBasedBoat p)
    {
        BoostTank_Data data = partData as BoostTank_Data;
        data.currentLevel = capacity;
    }

    public override void UpdatePart(BoatPartData partData, PartBasedBoat p)//constrain currentLevel between 0 and capacity
    {
        BoostTank_Data data = partData as BoostTank_Data;
        if(data.currentLevel < 0)
        {
            data.currentLevel = 0;
        }
        if(data.currentLevel > capacity)
        {
            data.currentLevel = capacity;
        }
    }
}
