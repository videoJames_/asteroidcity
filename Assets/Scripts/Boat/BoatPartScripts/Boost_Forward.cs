﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Boost_ForwardData : BoatPartData
{
    public EngineData engineData;
    public List<BoostTank_Data> boostTanks;
}
[CreateAssetMenu(menuName = "BoatParts/Boost/System")]
public class Boost_Forward : BoatPart
{
    public float baseSpeedModifier = 1, boostingSpeedModifier = 1.5f, burnRate = 1;

    public override BoatPartData CreateBoatPartData()
    {
        return new Boost_ForwardData();
    }

    public override void Initialize(BoatPartData partData, PartBasedBoat c)
    {
        Boost_ForwardData data = partData as Boost_ForwardData;
        data.engineData = c.allPartData.Find(x => x is EngineData) as EngineData; // find engine so we can read its speed (will return the first engine on the list)

        data.boostTanks = new List<BoostTank_Data>();
        List<BoatPartData> tankData = c.allPartData.FindAll(x => x is BoostTank_Data);
        foreach(BoatPartData d in tankData)
        {
            data.boostTanks.Add(d as BoostTank_Data); // get all tank data
        }

    }

    public override void UpdatePart(BoatPartData partData, PartBasedBoat c)
    {
        Boost_ForwardData data = partData as Boost_ForwardData;

        int tankToUse = -1; 
        //iterate through all tanks and look for first one with fluid left
        for(int i = data.boostTanks.Count - 1; i >= 0; i--)
        {
            if(data.boostTanks[i].currentLevel > 0)
            {
                tankToUse = i;
                i = -1;
            }
        }

        float m = baseSpeedModifier;
        if (tankToUse >= 0)
        {
            if (Input.GetKey(KeyCode.X) && c.boatPhys.touchingWater > .2f)
            {
                data.boostTanks[tankToUse].currentLevel -= Time.deltaTime * burnRate * c.boatPhys.touchingWater;
                m = boostingSpeedModifier * c.boatPhys.touchingWater;
            }
        }
        data.engineData.currentThrust *= m;
    }
}
