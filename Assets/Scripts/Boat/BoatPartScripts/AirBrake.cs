﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AirBrakeData : BoatPartData
{
    public List<BoostTank_Data> boostTanks;
    public RudderData rudderData;
    public EngineData engineData;

    //state
    public float releaseTimer = 0,
        currentBoostCharge;
}

[CreateAssetMenu(menuName = "BoatParts/Rudders/AirBrake")]
public class AirBrake : BoatPart
{
    public float upForce, height, brakeStrength, turnSpeedModifier, burnRate, boostStrength, boostDuration, dragMultiplier;
    [Range(.2f, 1f)]public  float timeWarpFactor;


    public override BoatPartData CreateBoatPartData() 
    {
        return new AirBrakeData();
    }

    public override void Initialize(BoatPartData partData, PartBasedBoat c)
    {

        AirBrakeData data = partData as AirBrakeData;
        data.rudderData = c.allPartData.Find(x => x is RudderData) as RudderData; // find rudder
        data.engineData = c.allPartData.Find(x => x is EngineData) as EngineData;//find engine so we can read its speed (will return the first engine on the list)

        data.boostTanks = new List<BoostTank_Data>();
        List<BoatPartData> tankData = c.allPartData.FindAll(x => x is BoostTank_Data);
        foreach (BoatPartData d in tankData)
        {
            data.boostTanks.Add(d as BoostTank_Data); // get all tank data
        }
    }

    public override void UpdatePart(BoatPartData partData, PartBasedBoat p)
    {
        AirBrakeData data = partData as AirBrakeData;
        float speedFactor = (p.boatPhys.rb.velocity.magnitude / data.engineData.boatPart.maxSpeed); //get this outta here!

        if (Input.GetKey(KeyCode.LeftShift))
        {
            Time.timeScale = Mathf.Lerp(Time.timeScale, timeWarpFactor, Time.unscaledDeltaTime * 1.5f);
            //       d.rate = Mathf.Lerp(d.rate, 3*startFramerate, Time.unscaledDeltaTime * 1.5f);

            if (data.currentBoostCharge < boostDuration)
                data.currentBoostCharge += Time.deltaTime * boostDuration * 1.5f;
            else
                data.currentBoostCharge = boostDuration;
        }
        else if (Input.GetKeyUp(KeyCode.LeftShift))
        {
            
            data.releaseTimer = data.currentBoostCharge;
            data.currentBoostCharge = 0;
        }
        else
        {
            data.releaseTimer -= Time.deltaTime;
            Time.timeScale = 1;
 //           d.rate = startFramerate;
        }


        int tankToUse = -1;
        //iterate through all tanks and look for first one with fluid left
        for (int i = data.boostTanks.Count - 1; i >= 0; i--)
        {
            if (data.boostTanks[i].currentLevel > 0)
            {
                tankToUse = i;
                i = -1;
            }
        }

        if (tankToUse >= 0)
        {
            if (data.releaseTimer > 0)
            {
                data.boostTanks[tankToUse].currentLevel -= burnRate * Time.deltaTime;

                Vector3 thrust = Vector3.Cross(p.boatPhys.t.right, p.boatPhys.lastWaterNormal).normalized;
                p.boatPhys.rb.AddForce( (data.releaseTimer / boostDuration) * thrust * boostStrength * 100 * Time.deltaTime,ForceMode.Acceleration);
            }
        }
    }

    public override void FixedUpdatePart(BoatPartData partData, PartBasedBoat p)
    {
        AirBrakeData data = partData as AirBrakeData;

        if (Input.GetKey(KeyCode.LeftShift))
        {
            p.boatPhys.currentBuoyancyForce *= upForce;
            p.boatPhys.currentFloatHeight *= height;
            p.boatPhys.rb.velocity /= Mathf.Clamp(1 + (brakeStrength / 100), 1,1000);
            data.rudderData.currentTurnRate *= turnSpeedModifier;
        }
    }
}
