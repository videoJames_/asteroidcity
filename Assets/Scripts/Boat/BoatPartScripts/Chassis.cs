﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "BoatParts/Chassis")]
public class Chassis : ScriptableObject
{
    public bool editMode = false;

    //config
    public Vector2 hoverPlane = new Vector2(1.5f, 3); //size of a rectangle, the corners are the hover points. always flat & aligned to the transform
    public float buoyancyForce = 11,
        waterHardness = .26f,
        floatingHeight = 2,
        airGravity = 4,
        airDrag = .3f,
        waterDrag = 1.5f,
        surfaceAngularDrag = 3.5f,
        antiCapsizeSpin = 10,
        restThreshold = 6,
        submergedSpinSpeed = -3;//rotate the boat by this angular force when underwater
    public bool useCapsizeProtection = true;
    public Vector3 waterContactPoint = new Vector3(0, -1, -3); //used to see if the engine is underwater
    
    //bookkeeping
    Buoyancy b;

    public void Initialize(GameObject obj)
    {
        b = obj.GetComponent<Buoyancy>();
        b.hoverPlane = hoverPlane;
        b.buoyancyForce = buoyancyForce;
        b.waterHardness = waterHardness;
        b.floatingHeight = floatingHeight;
        b.airGravity = airGravity;
        b.airDrag = airDrag;
        b.waterDrag = waterDrag;
        b.surfaceAngularDrag = surfaceAngularDrag;
        b.antiCapsizeSpin = antiCapsizeSpin;
        b.restThreshold = restThreshold;
        b.submergedSpinSpeed = submergedSpinSpeed;
        b.useCapsizeProtection = useCapsizeProtection;
        b.waterContactPoint = waterContactPoint;
    }

}
