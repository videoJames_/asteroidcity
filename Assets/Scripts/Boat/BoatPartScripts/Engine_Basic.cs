﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Engine_BasicData : EngineData
{
    public float accelFactor;
    public Engine_BasicData(BoatPart boatPart){this.boatPart = boatPart as EnginePart;}
}

[CreateAssetMenu(menuName = "BoatParts/Engines/Engine_Basic")]
public class Engine_Basic : EnginePart
{
    //config
    public float timeToTopSpeed = 15,
        reverseSpeedRatio = .4f;
    public AnimationCurve accelerationCurve;

    public override BoatPartData CreateBoatPartData()
    {
        return new Engine_BasicData(this as BoatPart);
    }

    public override void FixedUpdatePart(BoatPartData partData, PartBasedBoat c)
    {
        Engine_BasicData data = partData as Engine_BasicData; // move to initialize()

        if (Mathf.Abs(data.currentThrust) > .1f && c.boatPhys.touchingWater > .8f)
        {
            Vector3 thrustVector = Vector3.Cross(c.boatPhys.t.right, c.boatPhys.lastWaterNormal).normalized; // calculate thrust vector along the surface of the water by default

            RaycastHit hit;
            Vector3 ceil = c.boatPhys.t.position;
            ceil += -c.boatPhys.currentGravity.normalized * Buoyancy.raycastHeight; //get ceiling point above transform
            if (Physics.Raycast(ceil, c.boatPhys.currentGravity, out hit, Buoyancy.raycastHeight + c.boatPhys.floatingHeight, 1 << LayerMask.NameToLayer("Ramp"))) //cast down looking for a ramp
            {
                thrustVector = Vector3.Cross(c.boatPhys.t.right, hit.normal).normalized * 2f; // if on a ramp apply thrust along ramp's surface
            }

            //apply thrust
            c.boatPhys.rb.AddForce(thrustVector * data.currentThrust, ForceMode.Acceleration);
        }
    }
    

    public override void UpdatePart(BoatPartData partData, PartBasedBoat c)
    {
        Engine_BasicData data = partData as Engine_BasicData;
        //acceleration
        if (c.boatPhys.rb.velocity.magnitude >= c.boatPhys.lastVelocity.magnitude * .8f && c.accelInput > .2f && Mathf.Abs(c.turnInput) < .8f) //as long as the pedal is down & velocity doesn't decrease, keep accelerating
        {
            data.accelFactor = Mathf.Clamp01(data.accelFactor + (Time.deltaTime / timeToTopSpeed));
        }
        else
        {
            data.accelFactor = Mathf.Lerp(data.accelFactor, 0, Time.deltaTime * 2f);
        }

        //thrust
        if (c.boatPhys.touchingWater > .4f)
        {
            Debug.DrawLine(c.transform.TransformPoint(new Vector3(0,1,0)), c.transform.TransformPoint(c.boatPhys.waterContactPoint), Color.green);
            data.currentThrust = c.accelInput * maxSpeed * (1.5f * c.boatPhys.touchingWater);
            if (c.accelInput < -.1f)
                data.currentThrust *= reverseSpeedRatio;

            data.currentThrust *= accelerationCurve.Evaluate(data.accelFactor); //zoomiess
        }
        else
        {
            Debug.DrawLine(c.transform.TransformPoint(new Vector3(0, 1, 0)), c.transform.TransformPoint(c.boatPhys.waterContactPoint), Color.red);
            data.currentThrust = 0;
        }
    }
}
