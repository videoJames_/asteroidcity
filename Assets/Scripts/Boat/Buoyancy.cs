﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//uses 4 hover points to make game object float, also includes gravity and drag
[RequireComponent(typeof(Rigidbody))]
[DisallowMultipleComponent]
public class Buoyancy : MonoBehaviour
{
    //global
    [HideInInspector]
    public const float gravityChangeSpeed = 5.5f,//how fast to lerp to a new gravity direction
        submergedAngularDrag = 4,//how much drag to apply when underwater
        raycastHeight = 8,//how far above the boat to cast down for the water surface
        waterContactSmoothingSpeed = 4.5f;//how fast to adjust touchingWater (from 0 when out of water to 1 when submerged)

    //configuration 
    public Chassis chassis;

    //set by chassis object
    public float submergeForceThreshold, submergeRedirect, submergeUpForce;

    [HideInInspector]public Vector2 hoverPlane; //size of a rectangle, the corners are the hover points. always flat & aligned to the transform
    [HideInInspector]public float buoyancyForce,
        waterHardness,
        floatingHeight,
        airGravity,
        airDrag,
        waterDrag,
        surfaceAngularDrag,
        antiCapsizeSpin,
        restThreshold,
        submergedSpinSpeed;//rotate the boat by this angular force when underwater
    [HideInInspector]
    public bool useCapsizeProtection;
    [HideInInspector]
    public Vector3 waterContactPoint; //used to see if the engine/whatever is underwater
    
    //state tracking
    [HideInInspector]public Vector3 currentGravity;
    [HideInInspector]public float currentBuoyancyForce, currentFloatHeight, 
        touchingWater;//0 to 1. 1 means the waterContactPoint is submerged in the water. smoothed over time
    [HideInInspector]public Vector3 lastVelocity;
    bool bTouchingWater;//submerged this frame?

    //modifiers
    //used mainly by boat parts to alter buoyancy properties every frame. they are reset to 1 every frame (unsure if this will always work properly?)
    [HideInInspector]public float waterDragModifier;

    //bookkeeping
    [HideInInspector]public Rigidbody rb;
    [HideInInspector]public Transform t;
    [HideInInspector]public Vector3 targetGravity, lastWaterNormal;
    int waterMask, ceilMask;
    List<Collider> activeTriggers = new List<Collider>();
    [HideInInspector]public Vector3[] localHoverPoints;//first point is the center of the hoverPlane, other 4 are the corners

    void Start()
    {
        chassis.Initialize(gameObject);

        t = transform;
        rb = GetComponent<Rigidbody>();
        targetGravity = Physics.gravity;
        currentGravity = targetGravity;
        waterMask = 1 << LayerMask.NameToLayer("Water") | 1 << LayerMask.NameToLayer("Wall") | 1 << LayerMask.NameToLayer("Ramp");
        ceilMask = (1 << LayerMask.NameToLayer("Wall")) | (1 << LayerMask.NameToLayer("Ramp"));
        localHoverPoints = new Vector3[] { Vector3.zero, new Vector3(hoverPlane.x, 0, hoverPlane.y), new Vector3(-hoverPlane.x, 0, hoverPlane.y), new Vector3(-hoverPlane.x, 0, -hoverPlane.y), new Vector3(hoverPlane.x, 0, -hoverPlane.y) };
    }

    void Update()
    {
        UpdateTriggers();
        DetermineGravity();

        currentBuoyancyForce = buoyancyForce;
        currentFloatHeight = floatingHeight;
        waterDragModifier = 1;

        if (chassis.editMode)
            chassis.Initialize(gameObject);

        if(bTouchingWater)
        {
            touchingWater = Mathf.Lerp(touchingWater, 1, waterContactSmoothingSpeed * Time.deltaTime);
        }
        else
        {
            touchingWater = Mathf.Lerp(touchingWater, 0, waterContactSmoothingSpeed * Time.deltaTime);
        }

        rb.drag = Mathf.Lerp(airDrag, waterDrag * waterDragModifier, touchingWater);


    }

    void FixedUpdate()
    {
        ApplyBuoyancy();
        ApplyGravity();

        lastVelocity = Vector3.Lerp(lastVelocity, rb.velocity, .1f);
    }

    void ApplyBuoyancy()
    {
        bTouchingWater = false;
        float heightToUse = raycastHeight;
        rb.angularDrag = surfaceAngularDrag;

        RaycastHit hit;
        if (Physics.Raycast(t.TransformPoint(localHoverPoints[0]), -currentGravity, out hit, raycastHeight, ceilMask)) //look for a ceiling, if one is hit, cast from just below it
        {
            heightToUse = hit.distance;
        }

        for (int i = 0; i < localHoverPoints.Length; i++) //do a raycast for each hover point
        {
            Vector3 hoverPoint = t.TransformPoint(localHoverPoints[i]);
            Vector3 c = hoverPoint + (-currentGravity.normalized * heightToUse); // get ceiling point above hoverPoint


            if (Physics.Raycast(c, currentGravity, out hit, heightToUse + currentFloatHeight, waterMask)) //cast down from ceiling point
            {
                if (i == 0)//only used for the center point
                {
                    lastWaterNormal = hit.normal; //store the water normal for use in the thrust

                    if (hit.distance < heightToUse - (currentFloatHeight / 2))// boat is submerged
                    {
                        bTouchingWater = true;
                        rb.AddRelativeTorque(new Vector3(submergedSpinSpeed, 0, 0), ForceMode.Acceleration); //spin the boat upwards when underwater
                        rb.angularDrag = submergedAngularDrag; //apply extra angular drag when deeper below the water surface
                    }

                    //capsize protection
                    if (useCapsizeProtection && Vector3.Angle(t.up, currentGravity) < 90)
                    {
                        float d = Vector3.Dot(t.forward, currentGravity); //positive if tilted up, neg if not
                        d /= 90;

                        if (d > 0)
                            rb.AddRelativeTorque(new Vector3(-antiCapsizeSpin + ((1 - d) * 2), 0, 0), ForceMode.Acceleration);//spin boat upwards
                        else
                            rb.AddRelativeTorque(new Vector3(antiCapsizeSpin + ((1 - d) * 2), 0, 0), ForceMode.Acceleration);
                    }

                    float waterHitAngle = Vector3.Angle(rb.velocity, hit.normal);
                    if (rb.velocity.magnitude > submergeForceThreshold / 5 && waterHitAngle > 91 && hit.distance < heightToUse + (currentFloatHeight / 2))//apply extra downward drag when below the water surface
                    {
                        float strength = ((waterHitAngle - 91) / 50);//scale effect by angle between velocity and water normal

                        //this section redirects the velocity to be parallel to the water surface
                        Vector3 dragDir = hit.normal;
                        dragDir = Vector3.Reflect(rb.velocity, hit.normal);
                        dragDir = Vector3.Scale(dragDir, Vector3.one * submergeRedirect * (1.8f - strength));//invert strength to apply maximum redirect at shallower angles
                        dragDir = (dragDir + rb.velocity).normalized * rb.velocity.magnitude;
                        Vector3 redirectVel = dragDir;

                        //add an extra upward force based on angle
                        rb.AddForce(hit.normal * Mathf.Clamp01(strength * strength) * submergeUpForce, ForceMode.Acceleration);

                        //decrease velocity based on angle
                        strength = (1.8f - strength) / 1.8f;
                        strength = (strength / 5) + .8f;
                        Vector3 decayVel = rb.velocity * waterHardness * strength;

                        rb.velocity = Vector3.Lerp(decayVel, redirectVel, Mathf.Clamp01(rb.velocity.magnitude / submergeForceThreshold));
                    }
                }

                if (hit.distance < heightToUse - currentFloatHeight)
                {
                    //point is submerged (apply maximum force)
                    rb.AddForce(-currentGravity.normalized * currentBuoyancyForce, ForceMode.Acceleration);
                    rb.AddForceAtPosition(-currentGravity.normalized * currentBuoyancyForce, hoverPoint, ForceMode.Acceleration);//using hit.normal simulates a little wave slamming force but not gravity
                }
                else if (hit.distance < heightToUse || i != 0)
                {
                    //point is slightly submerged, apply force based on distance from water, angled away from the water
                    float force = currentBuoyancyForce * (1 - ((hit.distance - heightToUse + currentFloatHeight) / (2 * currentFloatHeight)));
                    rb.AddForceAtPosition(hit.normal * force, hoverPoint, ForceMode.Acceleration);//using hit.normal simulates a little wave slamming force but not gravity
                }
            }
        }


        if (!bTouchingWater && Physics.Raycast(t.TransformPoint(waterContactPoint) + (-currentGravity.normalized * heightToUse), currentGravity, out hit, heightToUse, waterMask)) //raycast to see if contactPoint is submerged
        {
            if (hit.distance < heightToUse)
            {
                bTouchingWater = true;
            }
        }
    }

    void ApplyGravity()
    {
        //anti-drift: when velocity is below a threshold, apply gravity based on water normal instead of currentGravity
        Vector3 currentGravityVector = currentGravity;

        if (rb.velocity.magnitude < restThreshold)
        {
            currentGravityVector = -lastWaterNormal;
        }

        currentGravityVector = currentGravityVector.normalized * currentGravity.magnitude;//constrain to gravity magnitude
        rb.AddForce(currentGravityVector, ForceMode.Acceleration);

        //apply extra gravity if in air
        rb.AddForce(currentGravity * airGravity * (1 - touchingWater), ForceMode.Acceleration);
        
    }

    void DetermineGravity()
    {
        currentGravity = Vector3.Lerp(currentGravity, targetGravity, Time.deltaTime * gravityChangeSpeed);// update local gravity vector
    }

    void UpdateTriggers()
    {
        targetGravity = Physics.gravity;
        for (int i = 0; i < activeTriggers.Count; i++)//can be extended to search for more zones (flowZone or waterCurrentZone?)
        {
            if (activeTriggers[i].CompareTag("GravityZone"))
            {
                targetGravity = -activeTriggers[i].transform.up * Physics.gravity.magnitude;// if we are in a gravity zone set targetGravity to its local down vector
            }
        }
    }
    void OnTriggerEnter(Collider col)
    {
        if (col.gameObject.layer == LayerMask.NameToLayer("Trigger") && !activeTriggers.Contains(col))
            activeTriggers.Add(col);
    }
    void OnTriggerExit(Collider col)
    {
        if (activeTriggers.Contains(col))
            activeTriggers.Remove(col);
    }
}
