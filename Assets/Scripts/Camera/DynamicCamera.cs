﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DynamicCamera : MonoBehaviour {

    public Buoyancy boat;
    public Transform lookTarget;
    public float distance, height, vertSmooth, followSmooth, lookHeight;

    Vector3 vertical, offset;

    void FixedUpdate ()
    {

        vertical = Vector3.Lerp(vertical, Vector3.Lerp(boat.lastWaterNormal, -boat.currentGravity, .1f), vertSmooth / 10);
        offset = Vector3.Lerp(offset, boat.transform.position + (Vector3.Cross(vertical, boat.transform.right).normalized * distance) + (vertical.normalized * height), followSmooth / 10);

        transform.position = offset;
        transform.LookAt(lookTarget.position + (boat.currentGravity.normalized * -lookHeight), vertical);
	}


}
